package com.example.usuario.primeraapp4b;

import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class ActividadFragmentos extends AppCompatActivity implements View.OnClickListener,  FragUno.OnFragmentInteractionListener, FragDos.OnFragmentInteractionListener {

    Button Fragmentouno, Fragmentodos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_fragmentos);
        Fragmentouno= (Button) findViewById(R.id.buttonFRAGMENTOUNO);
        Fragmentodos= (Button) findViewById(R.id.buttonFRAGMENTODOS);

        Fragmentouno.setOnClickListener(this);
        Fragmentodos.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonFRAGMENTOUNO:
                FragUno fragmentoUno = new FragUno();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.Contenedor,fragmentoUno);
                transaction.commit();
                break;
        }
        switch (v.getId()){
            case R.id.buttonFRAGMENTODOS:
                FragDos fragmentoDos = new FragDos();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.Contenedor,fragmentoDos);
                transaction.commit();
                break;

        }
    }
    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
